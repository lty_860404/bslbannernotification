//
//  BSLDemoViewController.h
//  BSLBannerNotificationDemo
//
//  Created by tianyin luo on 14-10-16.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSLBannerNotification.h"

@interface BSLDemoViewController : UIViewController<BSLNotificationViewDelegate>

@end
