//
//  BSLDemoViewController.m
//  BSLBannerNotificationDemo
//
//  Created by tianyin luo on 14-10-16.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLDemoViewController.h"
#import "BSLBannerNotificationView.h"
#import "UIView+AutoLayout.h"

@interface BSLDemoViewController ()
    @property (nonatomic) CGFloat offsetY;
@end

@implementation BSLDemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view.layer setBorderColor:[UIColor blueColor].CGColor];
    [self.view.layer setBorderWidth:2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)toggleNav:(id)sender
{
    BOOL isHide = ![self.navigationController isNavigationBarHidden];
    [self.navigationController setNavigationBarHidden:isHide
                                             animated:YES];
}

-(IBAction)showNotification:(id)sender
{
    NSLog(@"Show Test Info");
    [BSLBannerNotification showNotificationInViewController:self
                                                      title:@"Info"
                                                   subtitle:@"This is a Test"
                                                       type:BSLNotificationTypeWarning];
}

-(void) animationWithFadeIn:(BSLBannerNotificationView *)notificationView
{
    self.offsetY = CGRectGetHeight(notificationView.frame);
    [self.view setBounds:CGRectMake(self.view.bounds.origin.x,
                                    self.view.bounds.origin.y-self.offsetY,
                                    self.view.bounds.size.width,
                                    self.view.bounds.size.height)];
}

-(void) animationWithFadeOut:(BSLBannerNotificationView *)notificationView
{
    if(notificationView.position==BSLNotificationPositionTop && self.offsetY>0){
        [self.view setBounds:CGRectMake(self.view.bounds.origin.x,
                                        self.view.bounds.origin.y+self.offsetY,
                                        self.view.bounds.size.width,
                                        self.view.bounds.size.height+self.offsetY)];
        [self setContentSizeForViewInPopover:CGSizeMake(self.view.frame.size.width,self.view.frame.size.height+self.offsetY)];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(IBAction)actionWithTest:(id)sender
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
//                         [self.view setTransform:CGAffineTransformMakeTranslation(0, 200)];
                         [self.view setFrame:CGRectMake(self.view.frame.origin.x,
                                                        self.view.frame.origin.y+47.0,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height-47.0)];
                     } completion:^(BOOL finished) {
                         
                     }];
}


@end
