//
//  BSLBannerNotificationView.m
//  Pods
//
//  Created by tianyin luo on 14-10-16.
//
//

#import "BSLBannerNotificationView.h"
#import "HexColor.h"
#import "BSLBlurView.h"
#import <QuartzCore/QuartzCore.h>

#define BSLNotificationViewMinimunPadding 15.0
#define BSLDesignFileName @"NotificationDefaultDesign.json"
#define BSLNotificationBundleName @"BSLBannerNotification"

#define DESIGN_KEY_IMAGE @"imageName"
#define DESIGN_KEY_BACKGROUND_IMAGE @"backgroundImageName"
#define DESIGN_KEY_BACKGORUND_COLOR @"backgroundColor"
#define DESIGN_KEY_TEXT_COLOR @"textColor"
#define DESIGN_KEY_TITLE_FONT_SIZE @"titleFontSize"
#define DESIGN_KEY_TITLE_FONT_NAME @"titleFontName"
#define DESIGN_KEY_SHADOW_COLOR @"shadowColor"
#define DESIGN_KEY_SHADOW_OFFSET_X @"shadowOffsetX"
#define DESIGN_KEY_SHADOW_OFFSET_Y @"shadowOffsetY"
#define DESIGN_KEY_CONTENT_COLOR @"contentTextColor"
#define DESIGN_KEY_CONTENT_FONT_SIZE @"contentFontSize"
#define DESIGN_KEY_CONTENT_FONT_NAME @"contentFontName"
#define DESIGN_KEY_BTN_BACK_GROUND_IMAGE @"buttonBackgroundImageName"
#define DESIGN_KEY_NOTIFICATION_BTN_BACK_GROUND @"NotificationButtonBackground"
#define DESIGN_KEY_BTN_TITLE_SHADOW_COLOR @"buttonTitleShadowColor"
#define DESIGN_KEY_BTN_TITLE_TEXT_COLOR @"buttonTitleTextColor"
#define DESIGN_KEY_BTN_TITLE_SHADOW_OFFSET_X @"buttonTitleShadowOffsetX"
#define DESIGN_KEY_BTN_TITLE_SHADOW_OFFSET_Y @"buttonTitleShadowOffsetY"
#define DESIGN_KEY_BORDER_HEIGHT @"borderHeight"
#define DESIGN_KEY_BORDER_COLOR @"borderColor"

static NSMutableDictionary* notificationDesign;
static NSBundle* podBundle;

@interface BSLBannerNotification (BSLBannerNotificationView)
-(void) fadeOutNotification:(BSLBannerNotificationView*)currentview;
@end

@interface BSLBannerNotificationView()<UIGestureRecognizerDelegate>

@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSString* subTitle;
@property (nonatomic,strong) NSString* buttonTitle;
@property (nonatomic,strong) UIViewController* viewController;

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *contentLabel;
@property (nonatomic,strong) UIImageView* iconImageView;
@property (nonatomic,strong) UIButton* button;
@property (nonatomic,strong) UIView* borderView;
@property (nonatomic,strong) UIImageView* backgroundImageView;
@property (nonatomic,strong) BSLBlurView* backgroundBlurView;

@property (nonatomic,assign) CGFloat textSpaceLeft;
@property (nonatomic,assign) CGFloat textSpaceRight;

@property (copy) void (^callback)();
@property (copy) void (^buttonCallback)();

-(CGFloat) updateHeightOfMessageView;
-(void)layoutSubviews;

@end

@implementation BSLBannerNotificationView

+(NSMutableDictionary*) notificationDesign
{
    if(!notificationDesign){
        if(!podBundle){
            podBundle = [NSBundle bundleWithPath:
                         [[NSBundle mainBundle] pathForResource:BSLNotificationBundleName
                                                         ofType:@"bundle"]];
        
        }
        NSString* path =
            [[podBundle resourcePath] stringByAppendingPathComponent:BSLDesignFileName];
        notificationDesign = [NSMutableDictionary dictionaryWithDictionary:
                              [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path]
                                                              options:kNilOptions
                                                                error:nil]];
    }
    return notificationDesign;
}

+ (void) addNotificationDesignFromFile:(NSString *)filename
{
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    if([[NSFileManager defaultManager] fileExistsAtPath:path]){
        NSDictionary* design = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path]
                                                               options:kNilOptions
                                                                 error:nil];
        [[BSLBannerNotificationView notificationDesign] addEntriesFromDictionary:design];
    }else{
        NSAssert(NO, @"Error loading design file with name %@",filename);
    }
}

-(CGFloat)padding
{
    return self.position == BSLNotificationPositionNavBarOverlay?BSLNotificationViewMinimunPadding+10.0f:BSLNotificationViewMinimunPadding;
}

-(NSString*) currentTypeKey:(BSLNotificationType)notificationType
{
    NSString* result = nil;
    switch (notificationType) {
        case BSLNotificationTypeMessage:{
            result = @"message";
            break;
        }
        case BSLNotificationTypeError:{
            result = @"error";
            break;
        }
        case BSLNotificationTypeSuccess:{
            result = @"success";
            break;
        }
        case BSLNotificationTypeWarning:{
            result = @"warning";
            break;
        }
        default:
            break;
    }
    return result;
}

-(UIImage*) bundleImageForFileName:(NSString*)fileName
{
    if(fileName==nil){
        return nil;
    }
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@.bundle/%@",BSLNotificationBundleName,fileName]];
}

-(instancetype) initWithTitle:(NSString*)title
                     subtitle:(NSString*)subTitle
                        image:(UIImage*)image
                         type:(BSLNotificationType)notificationType
                     duration:(CGFloat)duration
             inViewController:(UIViewController*)viewController
                     callback:(void (^)())callback
                  buttonTitle:(NSString*)buttonTitle
               buttonCallback:(void (^)())buttonCallback
                   atPosition:(BSLNotificationPosition)position
         canBeDismissedByUser:(BOOL)dismissingEnabled
{
    NSDictionary* notificationDesign = [BSLBannerNotificationView notificationDesign];
    self = [self init];
    if(self){
        self.title = title;
        self.subTitle = subTitle;
        self.buttonTitle = buttonTitle;
        self.duration = duration;
        self.viewController = viewController;
        self.position = position;
        self.callback = callback;
        self.buttonCallback = buttonCallback;
        
        NSDictionary* current = [notificationDesign valueForKey:[self currentTypeKey:notificationType]];
        
        if(!image && [[current valueForKey:DESIGN_KEY_IMAGE] length])
        {
            image = [self bundleImageForFileName:[current valueForKey:DESIGN_KEY_IMAGE]];
        }
        
        if(![BSLBannerNotification iOS7StyleEnabled]){
            self.alpha = 0.0;
            UIImage* backgroundImage = [self bundleImageForFileName:[current valueForKey:DESIGN_KEY_BACKGROUND_IMAGE]];
            backgroundImage = [backgroundImage stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0];
            
            self.backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
            self.backgroundImageView.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
            [self addSubview:self.backgroundImageView];
        }
        else
        {
            self.backgroundBlurView = [[BSLBlurView alloc] init];
            self.backgroundBlurView.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
            self.backgroundBlurView.blurTintColor = [UIColor colorWithHexString:[current valueForKey:DESIGN_KEY_BACKGORUND_COLOR]];
            [self addSubview:self.backgroundBlurView];
        }
        
        CGFloat padding = [self padding];
        self.textSpaceLeft = 2*padding;
        if(image) {
            self.textSpaceLeft += image.size.width + 2*padding;
        }
        
        UIColor* fontColor = [UIColor colorWithHexString:[current valueForKey:DESIGN_KEY_TEXT_COLOR] alpha:1.0];
        self.titleLabel = [[UILabel alloc] init];
        [self.titleLabel setText:title];
        [self.titleLabel setTextColor:fontColor];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        CGFloat fontSize = [[current valueForKey:DESIGN_KEY_TITLE_FONT_SIZE] floatValue];
        NSString* fontName = [current valueForKey:DESIGN_KEY_TITLE_FONT_NAME];
        if(fontName != nil){
            [self.titleLabel setFont:[UIFont fontWithName:fontName size:fontSize]];
        }else{
            [self.titleLabel setFont:[UIFont boldSystemFontOfSize:fontSize]];
        }
        [self.titleLabel setShadowColor:[UIColor colorWithHexString:[current valueForKey:DESIGN_KEY_SHADOW_COLOR] alpha:1.0]];
        [self.titleLabel setShadowOffset:CGSizeMake([[current valueForKey:DESIGN_KEY_SHADOW_OFFSET_X] floatValue],
                                                    [[current valueForKey:DESIGN_KEY_SHADOW_OFFSET_Y] floatValue])];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:self.titleLabel];
        
        if([subTitle length]){
            self.contentLabel = [[UILabel alloc] init];
            [self.contentLabel setText:subTitle];
            
            UIColor* contentTextColor = [UIColor colorWithHexString:[current valueForKey:DESIGN_KEY_CONTENT_COLOR] alpha:1.0];
            if(!contentTextColor){
                contentTextColor = fontColor;
            }
            [self.contentLabel setTextColor:contentTextColor];
            [self.contentLabel setBackgroundColor:[UIColor clearColor]];
            CGFloat fontSize = [[current valueForKey:DESIGN_KEY_CONTENT_FONT_SIZE] floatValue];
            NSString* fontName = [current valueForKey:DESIGN_KEY_CONTENT_FONT_NAME];
            if(fontName!=nil){
                [self.contentLabel setFont:[UIFont fontWithName:fontName size:fontSize]];
            }else{
                [self.contentLabel setFont:[UIFont boldSystemFontOfSize:fontSize]];
            }
            [self.contentLabel setShadowColor:self.titleLabel.shadowColor];
            [self.contentLabel setShadowOffset:self.titleLabel.shadowOffset];
            self.contentLabel.numberOfLines = 0;
            self.contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
            [self addSubview:self.contentLabel];
        }
        
        CGFloat screenWidth = self.viewController.view.bounds.size.width;
        
        if(image){
            self.iconImageView = [[UIImageView alloc] initWithImage:image];
            self.iconImageView.frame = CGRectMake(padding*2,
                                                  padding,
                                                  image.size.width,
                                                  image.size.height);
            [self addSubview:self.iconImageView];
        }
        
        if([buttonTitle length]){
            self.button = [UIButton buttonWithType:UIButtonTypeCustom];
            
            UIImage* buttonBackgroundImage = [self bundleImageForFileName:[current valueForKey:DESIGN_KEY_BTN_BACK_GROUND_IMAGE]];
            buttonBackgroundImage = [buttonBackgroundImage resizableImageWithCapInsets:
                                     UIEdgeInsetsMake(15.0, 22.0, 15.0, 11.0)];
            
            if(!buttonBackgroundImage){
                buttonBackgroundImage = [self bundleImageForFileName:[current valueForKey:DESIGN_KEY_NOTIFICATION_BTN_BACK_GROUND]];
                buttonBackgroundImage = [buttonBackgroundImage resizableImageWithCapInsets:
                                         UIEdgeInsetsMake(15.0, 22.0, 15.0, 11.0)];
            }
            
            [self.button setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
            self.button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
            self.button.titleLabel.shadowOffset =
            CGSizeMake([[current valueForKey:DESIGN_KEY_BTN_TITLE_SHADOW_OFFSET_X] floatValue],
                       [[current valueForKey:DESIGN_KEY_BTN_TITLE_SHADOW_OFFSET_Y] floatValue]);
            [self.button addTarget:self
                            action:@selector(buttonTapped:)
                  forControlEvents:UIControlEventTouchUpInside];
            
            self.button.contentEdgeInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0);
            [self.button sizeToFit];
            self.button.frame = CGRectMake(screenWidth-padding-self.button.frame.size.width,
                                           0.0,
                                           self.button.frame.size.width,
                                           31.0);
            [self addSubview:self.button];
            
            self.textSpaceRight = self.button.frame.size.width+padding;
        }
        
        if(![BSLBannerNotification iOS7StyleEnabled]){
            self.borderView =
            [[UIView alloc] initWithFrame:CGRectMake(0.0,
                                                     0.0,
                                                     screenWidth,
                                                     [[current valueForKey:DESIGN_KEY_BORDER_HEIGHT]
                                                                                            floatValue])];
            self.borderView.backgroundColor =
            [UIColor colorWithHexString:[current valueForKey:DESIGN_KEY_BORDER_COLOR] alpha:1.0];
            self.borderView.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
            [self addSubview:self.borderView];
        }
        
        CGFloat actualHeight = [self updateHeightOfMessageView];
        CGFloat topPosition = -actualHeight;
        
        if(self.position == BSLNotificationPositionBottom){
            topPosition = self.viewController.view.bounds.size.height;
        }
        
        self.frame = CGRectMake(0.0, topPosition, screenWidth, actualHeight);
        
        if(self.position == BSLNotificationPositionTop){
            self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        }else{
            self.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin);
        }
        
        if(dismissingEnabled){
            UISwipeGestureRecognizer *gestureRec =
                [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(fadeMeOut)];
            [gestureRec setDirection:(self.position == BSLNotificationPositionTop?UISwipeGestureRecognizerDirectionUp:UISwipeGestureRecognizerDirectionDown)];
            [self addGestureRecognizer:gestureRec];
            
            UITapGestureRecognizer *tapRec =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fadeMeOut)];
            [self addGestureRecognizer:tapRec];
        }
        
        if(self.callback){
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            tapGesture.delegate = self;
            [self addGestureRecognizer:tapGesture];
        }
    }
    return self;
}

-(CGFloat) updateHeightOfMessageView
{
    CGFloat currentHeight;
    CGFloat screenWidth = self.viewController.view.bounds.size.width;
    CGFloat padding = [self padding];

    self.titleLabel.frame = CGRectMake(self.textSpaceLeft,
                                       padding,
                                       screenWidth-padding-self.textSpaceLeft-self.textSpaceRight,
                                       0.0);
    [self.titleLabel sizeToFit];
    
    if([self.subTitle length]){
        self.contentLabel.frame =
        CGRectMake(self.textSpaceLeft,
                   self.titleLabel.frame.origin.y+self.titleLabel.frame.size.height+5.0,
                   screenWidth - padding - self.textSpaceLeft - self.textSpaceRight,
                   0.0);
        [self.contentLabel sizeToFit];
        currentHeight = self.contentLabel.frame.origin.y+self.contentLabel.frame.size.height;
    }else{
        currentHeight = self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height;
    }
    currentHeight += padding;
    
    if(self.iconImageView){
        if(self.iconImageView.frame.origin.y+self.iconImageView.frame.size.height+padding>currentHeight){
            currentHeight = self.iconImageView.frame.origin.y+self.iconImageView.frame.size.height+padding;
        }else{
            self.iconImageView.center = CGPointMake([self.iconImageView center].x,
                                                    round(currentHeight/2.0));
        }
    }
    
    self.button.center = CGPointMake([self.button center].x,
                                     round(currentHeight/2.0));
    
    if(self.position == BSLNotificationPositionTop)
    {
        CGRect borderFrame = self.borderView.frame;
        borderFrame.origin.y=currentHeight;
        self.borderView.frame = borderFrame;
    }
    
    currentHeight += self.borderView.frame.size.height;
    
    self.frame = CGRectMake(0.0, self.frame.origin.y, self.frame.size.width, currentHeight);
    
    if(self.button){
        self.button.frame = CGRectMake(self.frame.size.width-self.textSpaceRight,
                                       round((self.frame.size.height/2.0)-self.button.frame.size.height/2.0),
                                       self.button.frame.size.width,
                                       self.button.frame.size.height);
    }
    
    CGRect backgroundFrame = CGRectMake(self.backgroundImageView.frame.origin.x,
                                        self.backgroundImageView.frame.origin.y,
                                        screenWidth,
                                        currentHeight);
    
    if([BSLBannerNotification iOS7StyleEnabled]){
        if(self.position==BSLNotificationPositionTop){
            float topOffset = 0.f;
            
            UINavigationController* navigationController = self.viewController.navigationController;
            if(!navigationController && [self.viewController isKindOfClass:[UINavigationController class]]){
                navigationController = (UINavigationController*)self.viewController;
            }
            BOOL isNavBarIsHide = !navigationController || [BSLBannerNotification isNavigationBarInNavigationControllerHidden:navigationController];
            BOOL isNavBarIsOpaque = !navigationController.navigationBar.isTranslucent && navigationController.navigationBar.alpha==1;
            
            if(isNavBarIsHide||isNavBarIsOpaque){
                topOffset = -30.f;
            }
            backgroundFrame = UIEdgeInsetsInsetRect(backgroundFrame, UIEdgeInsetsMake(topOffset, 0.f, 0.f, 0.f));

        }else if(self.position == BSLNotificationPositionBottom){
            backgroundFrame = UIEdgeInsetsInsetRect(backgroundFrame, UIEdgeInsetsMake(0.f, 0.f, -30.f, 0.f));
        }
    }
    self.backgroundImageView.frame = backgroundFrame;
    self.backgroundBlurView.frame = backgroundFrame;
    
    return currentHeight;
}


- (void)buttonTapped:(id) sender
{
    if (self.buttonCallback)
    {
        self.buttonCallback();
    }
    
    [self fadeMeOut];
}

-(void)handleTap:(UITapGestureRecognizer*)tapGesture
{
    if(tapGesture.state == UIGestureRecognizerStateRecognized){
        if(self.callback){
            self.callback();
        }
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return !([touch.view isKindOfClass:[UIControl class]]);
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    [self updateHeightOfMessageView];
}

-(void) fadeMeOut
{
    [[BSLBannerNotification shared] performSelectorOnMainThread:@selector(fadeOutNotification:)
                                                     withObject:self
                                                  waitUntilDone:NO];
}

-(void) didMoveToWindow
{
    [super didMoveToWindow];
    if(self.duration==BSLNotificationDurationEndless && self.superview && !self.window){
        [self fadeMeOut];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
