//
//  BSLBlurView.m
//  Pods
//
//  Created by tianyin luo on 14-10-16.
//
//

#import "BSLBlurView.h"
#import <QuartzCore/QuartzCore.h>

@interface BSLBlurView()

    @property (strong,nonatomic) UIToolbar* toolbar;

@end

@implementation BSLBlurView

-(UIToolbar*) toolbar
{
    if(_toolbar==nil){
        _toolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
        _toolbar.userInteractionEnabled = NO;
        _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
        [_toolbar setBackgroundImage:nil
                      forToolbarPosition:UIBarPositionAny
                              barMetrics:UIBarMetricsDefault];
#endif
        [self addSubview:_toolbar];
    }
    return _toolbar;
}

-(void) setBlurTintColor:(UIColor*)blurTintColor
{
    if([self.toolbar respondsToSelector:@selector(setBarTintColor:)]){
        [self.toolbar performSelector:@selector(setBarTintColor:)
                           withObject:blurTintColor ];
    }
}

-(UIColor*)blurTintColor
{
    if([self.toolbar respondsToSelector:@selector(barTintColor)]){
        return [self.toolbar performSelector:@selector(barTintColor)];
    }
    return nil;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
