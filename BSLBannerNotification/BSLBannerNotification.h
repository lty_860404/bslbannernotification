//
//  BSLBannerNotification.h
//  BSLBannerNotification
//
//  Created by tianyin luo on 14-10-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

// NS_ENUM is now the preferred way to do typedefs. It gives the compiler and debugger more information, which helps everyone.
// When using SDK 6 or later, NS_ENUM is defined by Apple, so this block does nothing.
// For SDK 5 or earlier, this is the same definition block Apple uses.
#ifndef NS_ENUM
#if (__cplusplus && __cplusplus >= 201103L && (__has_extension(cxx_strong_enums) || __has_feature(objc_fixed_enum))) || (!__cplusplus && __has_feature(objc_fixed_enum))
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#if (__cplusplus)
#define NS_OPTIONS(_type, _name) _type _name; enum : _type
#else
#define NS_OPTIONS(_type, _name) enum _name : _type _name; enum _name : _type
#endif
#else
#define NS_ENUM(_type, _name) _type _name; enum
#define NS_OPTIONS(_type, _name) _type _name; enum
#endif
#endif

#define TS_SYSTEM_VERSION_LESS_THAN(v)            ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@class BSLBannerNotificationView;

@protocol BSLNotificationViewDelegate <NSObject>

-(void) animationWithFadeIn:(BSLBannerNotificationView*)notificationView;
-(void) animationWithFadeOut:(BSLBannerNotificationView*)notificationView;
-(void) completedWithFadeIn:(BSLBannerNotificationView*)notificationView;
-(void) completedWithFadeOut:(BSLBannerNotificationView*)notificationView;
-(BOOL) autoFadeOut;

@end

typedef NS_ENUM(NSInteger,BSLNotificationType){
    BSLNotificationTypeMessage=0,
    BSLNotificationTypeWarning,
    BSLNotificationTypeError,
    BSLNotificationTypeSuccess
};
typedef NS_ENUM(NSInteger,BSLNotificationPosition){
    BSLNotificationPositionTop=0,
    BSLNotificationPositionNavBarOverlay,
    BSLNotificationPositionBottom
};
typedef NS_ENUM(NSInteger,BSLNotificationDuration){
    BSLNotificationDurationAutomatic=0,
    BSLNotificationDurationEndless=-1
};

@interface BSLBannerNotification : NSObject

+ (UIViewController *)defaultViewController;

+ (BSLBannerNotification*) shared;

+ (void)showNotificationWithTitle:(NSString *)title
                             type:(BSLNotificationType)type;

+ (void)showNotificationWithTitle:(NSString *)title
                         subtitle:(NSString *)subtitle
                             type:(BSLNotificationType)type;

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    type:(BSLNotificationType)type
                                duration:(NSTimeInterval)duration;

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    type:(BSLNotificationType)type
                                duration:(NSTimeInterval)duration
                    canBeDismissedByUser:(BOOL)dismissingEnabled;

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    type:(BSLNotificationType)type;

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                   image:(UIImage *)image
                                    type:(BSLNotificationType)type
                                duration:(NSTimeInterval)duration
                                callback:(void (^)())callback
                             buttonTitle:(NSString *)buttonTitle
                          buttonCallback:(void (^)())buttonCallback
                              atPosition:(BSLNotificationPosition)messagePosition
                    canBeDismissedByUser:(BOOL)dismissingEnabled;

+ (BOOL)iOS7StyleEnabled;

+ (BOOL)isNavigationBarInNavigationControllerHidden:(UINavigationController *)navController;

+ (BOOL)dismissActiveNotification;

+ (BOOL)dismissActiveNotificationWithCompletion:(void (^)())completion;

+ (void)setDefaultViewController:(UIViewController *)defaultViewController;

+ (void)addCustomDesignFromFileWithName:(NSString *)fileName;

+ (BOOL)isNotificationActive;

+ (NSArray*) queuedMessages;

+ (void) prepareNotificationToBeShown:(BSLBannerNotificationView*)notificationView;

@end
