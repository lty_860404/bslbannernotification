//
//  BSLBlurView.h
//  Pods
//
//  Created by tianyin luo on 14-10-16.
//
//

#import <UIKit/UIKit.h>

@interface BSLBlurView : UIView

@property (strong,nonatomic) UIColor* blurTintColor;

@end
