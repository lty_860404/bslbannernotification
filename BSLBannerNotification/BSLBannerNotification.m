//
//  BSLBannerNotification.m
//  BSLBannerNotification
//
//  Created by tianyin luo on 14-10-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLBannerNotification.h"
#import "BSLBannerNotificationView.h"
#import <QuartzCore/QuartzCore.h>

#define NotificationDisplayTime 3.0
#define NotificationExtraDisplayTimePerPixel 0.04
#define NotificationAnimationDuration 0.3

static BOOL useIOS7Style;
static BOOL notificationActive;

__weak static UIViewController* defaultViewController;

@interface BSLBannerNotification()

@property (strong,nonatomic) NSMutableArray* message;

@end

@implementation BSLBannerNotification

static BSLBannerNotification* sharedNotification;

-(instancetype)init{
    self = [super init];
    if(self){
        _message = [[NSMutableArray alloc] init];
    }
    return self;
}

+(BSLBannerNotification*) shared
{
    if(!sharedNotification){
        sharedNotification = [[BSLBannerNotification alloc] init];
    }
    return sharedNotification;
}

+ (void)showNotificationWithTitle:(NSString *)title
                             type:(BSLNotificationType)type
{
    [self showNotificationWithTitle:title
                           subtitle:nil
                               type:type];
}

+ (void)showNotificationWithTitle:(NSString *)title
                         subtitle:(NSString *)subtitle
                             type:(BSLNotificationType)type
{
    [self showNotificationInViewController:[self defaultViewController]
                                     title:title
                                  subtitle:subtitle
                                      type:type];
}

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    type:(BSLNotificationType)type
                                duration:(NSTimeInterval)duration
{
    [self showNotificationInViewController:viewController
                                     title:title
                                  subtitle:subtitle
                                     image:nil
                                      type:type
                                  duration:duration
                                  callback:nil
                               buttonTitle:nil
                            buttonCallback:nil
                                atPosition:BSLNotificationPositionTop
                      canBeDismissedByUser:YES];
}

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    type:(BSLNotificationType)type
                                duration:(NSTimeInterval)duration
                    canBeDismissedByUser:(BOOL)dismissingEnabled
{
    [self showNotificationInViewController:viewController
                                     title:title
                                  subtitle:subtitle
                                     image:nil
                                      type:type
                                  duration:duration
                                  callback:nil
                               buttonTitle:nil
                            buttonCallback:nil
                                atPosition:BSLNotificationPositionTop
                      canBeDismissedByUser:dismissingEnabled];
}

+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                    type:(BSLNotificationType)type
{
    [self showNotificationInViewController:viewController
                                     title:title
                                  subtitle:subtitle
                                     image:nil
                                      type:type
                                  duration:BSLNotificationDurationAutomatic
                                  callback:nil
                               buttonTitle:nil
                            buttonCallback:nil
                                atPosition:BSLNotificationPositionTop
                      canBeDismissedByUser:YES];
}


+ (void)showNotificationInViewController:(UIViewController *)viewController
                                   title:(NSString *)title
                                subtitle:(NSString *)subtitle
                                   image:(UIImage *)image
                                    type:(BSLNotificationType)type
                                duration:(NSTimeInterval)duration
                                callback:(void (^)())callback
                             buttonTitle:(NSString *)buttonTitle
                          buttonCallback:(void (^)())buttonCallback
                              atPosition:(BSLNotificationPosition)messagePosition
                    canBeDismissedByUser:(BOOL)dismissingEnabled
{
    BSLBannerNotificationView *v = [[BSLBannerNotificationView alloc] initWithTitle:title
                                                   subtitle:subtitle
                                                      image:image
                                                       type:type
                                                   duration:duration
                                           inViewController:viewController
                                                   callback:callback
                                                buttonTitle:buttonTitle
                                             buttonCallback:buttonCallback
                                                 atPosition:messagePosition
                                       canBeDismissedByUser:dismissingEnabled];
    [self prepareNotificationToBeShown:v];
}

+ (BOOL)iOS7StyleEnabled
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Decide wheter to use iOS 7 style or not based on the running device and the base sdk
        BOOL iOS7SDK = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
        iOS7SDK = YES;
#endif
        useIOS7Style = ! (TS_SYSTEM_VERSION_LESS_THAN(@"7.0") || !iOS7SDK);
    });
    return useIOS7Style;
}

+ (BOOL)isNavigationBarInNavigationControllerHidden:(UINavigationController *)navController
{
    if([navController isNavigationBarHidden]) {
        return YES;
    } else if ([[navController navigationBar] isHidden]) {
        return YES;
    } else {
        return NO;
    }
}

-(void) fadeOutNotification:(BSLBannerNotificationView*)currentview
{
    [self fadeOutNotification:currentview
       animationFinishedBlock:nil];
}

-(void) fadeOutNotification:(BSLBannerNotificationView*)currentview
     animationFinishedBlock:(void (^)())animationFinished
{
    currentview.messageIsFullyDisplayed = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(fadeOutNotification:)
                                               object:currentview];
    CGPoint fadeOutToPoint;
    if(currentview.position != BSLNotificationPositionBottom){
        fadeOutToPoint = CGPointMake(currentview.center.x,-CGRectGetHeight(currentview.frame)/2.f);
    }else{
        fadeOutToPoint = CGPointMake(currentview.center.x,
                                     currentview.viewController.view.bounds.size.height+CGRectGetHeight(currentview.frame)/2.0);
    }
    
    [UIView animateWithDuration:NotificationAnimationDuration
                     animations:^{
                         
                         currentview.center = fadeOutToPoint;
                         if(![BSLBannerNotification iOS7StyleEnabled]){
                             currentview.alpha = 0.f;
                         }
                         
                         if([currentview.viewController conformsToProtocol:@protocol(BSLNotificationViewDelegate)]
                            &&[currentview.viewController respondsToSelector:@selector(animationWithFadeOut:)]){
                             [currentview.viewController performSelector:@selector(animationWithFadeOut:)
                                                              withObject:currentview];
                         }
                     } completion:^(BOOL finished) {
                         if([currentview.viewController conformsToProtocol:@protocol(BSLNotificationViewDelegate)]
                            &&[currentview.viewController respondsToSelector:@selector(completedWithFadeOut:)]){
                             [currentview.viewController performSelector:@selector(completedWithFadeOut:)
                                                              withObject:currentview];
                         }
                         
                         [currentview removeFromSuperview];
                         
                         if([self.message count]>0){
                             [self.message removeObjectAtIndex:0];
                         }
                         
                         notificationActive = NO;
                         
                         if([self.message count]>0){
                             [self fadeInCurrentNotification];
                         }
                         
                         if(animationFinished){
                             animationFinished();
                         }
                     }];
    
}


-(void) fadeInCurrentNotification
{
    if(self.message.count==0) return;
    notificationActive=YES;
    
    BSLBannerNotificationView* currentView = [self.message objectAtIndex:0];
    __block CGFloat verticalOffset = 0.0f;
    
    void(^addStatusBarHeightToVerticalOffset)()=^void(){
        if(currentView.position==BSLNotificationPositionNavBarOverlay){
            return;
        }
        CGSize statusBarSize = [UIApplication sharedApplication].statusBarFrame.size;
        if([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]){
            verticalOffset += statusBarSize.height;
        }else{
            BOOL isPortrait = UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
            CGFloat offset = isPortrait?statusBarSize.height:statusBarSize.width;
            verticalOffset+=offset;
        }
    };
    
    if([currentView.viewController isKindOfClass:[UINavigationController class]]
       ||[currentView.viewController.parentViewController isKindOfClass:[UINavigationController class]]){
        UINavigationController* currentNavigationController;
        
        if([currentView.viewController isKindOfClass:[UINavigationController class]]){
            currentNavigationController = (UINavigationController*)currentView.viewController;
        }else{
            currentNavigationController = (UINavigationController*)currentView.viewController.parentViewController;
        }
        
        BOOL isViewIsUnderStatusBar = [[[currentNavigationController childViewControllers] firstObject] wantsFullScreenLayout];
        if(!isViewIsUnderStatusBar && currentNavigationController.parentViewController==nil){
            isViewIsUnderStatusBar = ![BSLBannerNotification isNavigationBarInNavigationControllerHidden:currentNavigationController];
        }
        if(![BSLBannerNotification isNavigationBarInNavigationControllerHidden:currentNavigationController]
           && currentView.position != BSLNotificationPositionNavBarOverlay){
            [currentNavigationController.view insertSubview:currentView
                                               belowSubview:[currentNavigationController navigationBar]];
            verticalOffset = [currentNavigationController navigationBar].bounds.size.height;
            if([BSLBannerNotification iOS7StyleEnabled]||isViewIsUnderStatusBar){
                addStatusBarHeightToVerticalOffset();
            }
        }else{
            [currentView.viewController.view addSubview:currentView];
            if([BSLBannerNotification iOS7StyleEnabled]||isViewIsUnderStatusBar){
                addStatusBarHeightToVerticalOffset();
            }
        }
    }else{
        [currentView.viewController.view addSubview:currentView];
        if([BSLBannerNotification iOS7StyleEnabled]){
            addStatusBarHeightToVerticalOffset();
        }
    }
    
    CGPoint toPoint;
    if(currentView.position != BSLNotificationPositionBottom){
        CGFloat navigationbarButtomOfViewController = 0.0;
        if(currentView.delegate && [currentView.delegate respondsToSelector:@selector(navigationbarBottomOfViewController:)])
        {
            navigationbarButtomOfViewController = [currentView.delegate navigationbarBottomOfViewController:currentView.viewController];
        }
        toPoint = CGPointMake(currentView.center.x,
                              navigationbarButtomOfViewController+verticalOffset+CGRectGetHeight(currentView.frame)/2.0);
    }else{
        CGFloat y = currentView.viewController.view.bounds.size.height-CGRectGetHeight(currentView.frame)/2.0;
        if(!currentView.viewController.navigationController.isToolbarHidden){
            y -= CGRectGetHeight(currentView.viewController.navigationController.toolbar.bounds);
        }
        toPoint = CGPointMake(currentView.center.x, y);
    }
    
    dispatch_block_t animationBlock = ^{
        currentView.center = toPoint;
        if(![BSLBannerNotification iOS7StyleEnabled]){
            currentView.alpha = BSLBannerNotificationAlpha;
        }
        if([currentView.viewController conformsToProtocol:@protocol(BSLNotificationViewDelegate)]
           &&[currentView.viewController respondsToSelector:@selector(animationWithFadeIn:)]){
            [currentView.viewController performSelector:@selector(animationWithFadeIn:)
                                             withObject:currentView];
        }
    };
    void(^completionBlock)(BOOL) = ^(BOOL finished){
        currentView.messageIsFullyDisplayed = YES;
        if([currentView.viewController conformsToProtocol:@protocol(BSLNotificationViewDelegate)]
           &&[currentView.viewController respondsToSelector:@selector(completedWithFadeIn:)]){
            [currentView.viewController performSelector:@selector(completedWithFadeIn:)
                                             withObject:currentView];
        }
    };
    
    if(![BSLBannerNotification iOS7StyleEnabled]){
        [UIView animateWithDuration:NotificationAnimationDuration
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionAllowUserInteraction
                         animations:animationBlock
                         completion:completionBlock];
    }else{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >=70000
        [UIView animateWithDuration:NotificationAnimationDuration+0.1
                              delay:0.0
             usingSpringWithDamping:0.8
              initialSpringVelocity:0.f
                            options:UIViewAnimationOptionCurveEaseInOut|UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionAllowUserInteraction
                         animations:animationBlock
                         completion:completionBlock];
#endif
    }

    if(currentView.duration == BSLNotificationDurationAutomatic){
        currentView.duration = NotificationAnimationDuration + NotificationDisplayTime + currentView.frame.size.height*NotificationExtraDisplayTimePerPixel;
    }
    
    if(currentView.duration != BSLNotificationDurationEndless){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(fadeOutNotification:)
                       withObject:currentView
                       afterDelay:currentView.duration];
        });
    }
}

+ (BOOL)dismissActiveNotification
{
    return [self dismissActiveNotificationWithCompletion:nil];
}

+ (BOOL)dismissActiveNotificationWithCompletion:(void (^)())completion
{
    if([BSLBannerNotification shared].message.count==0) return NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        if([BSLBannerNotification shared].message.count==0) return;
        BSLBannerNotificationView* currentView = [[BSLBannerNotification shared].message objectAtIndex:0];
        if(currentView.messageIsFullyDisplayed){
            [[BSLBannerNotification shared] fadeOutNotification:currentView
                                         animationFinishedBlock:completion];
        }
    });
    return YES;
}

+ (void)setDefaultViewController:(UIViewController *)viewController
{
    defaultViewController = viewController;
}

+ (void)addCustomDesignFromFileWithName:(NSString *)fileName
{
    [BSLBannerNotificationView addNotificationDesignFromFile:fileName];
}

+ (BOOL)isNotificationActive
{
    return notificationActive;
}

+ (NSArray*) queuedMessages
{
    return [BSLBannerNotification shared].message;
}

+ (UIViewController*)defaultViewController
{
    __strong UIViewController *viewController = defaultViewController;
    if(!viewController){
        NSLog(@"BSLBannerNotification:It is recommended to set a customer defaultViewController that is used to display the notification!");
        viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    }
    return viewController;
}

+(void) prepareNotificationToBeShown:(BSLBannerNotificationView *)notificationView
{
    NSString* title = notificationView.title;
    NSString* subTitle = notificationView.subTitle;
    
    for(BSLBannerNotificationView* n in [BSLBannerNotification shared].message){
        if( ([n.title isEqualToString:title] || (!n.title && !title))
           &&([n.subTitle isEqualToString:subTitle] || (!n.subTitle&&!subTitle)) ){
            return;
        }
    }
    
    [[BSLBannerNotification shared].message addObject:notificationView];
    if(!notificationActive){
        [[BSLBannerNotification shared] fadeInCurrentNotification];
    }
}

@end
