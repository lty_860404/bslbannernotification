//
//  BSLBannerNotificationView.h
//  Pods
//
//  Created by tianyin luo on 14-10-16.
//
//

#import <UIKit/UIKit.h>
#import "BSLBannerNotification.h"

#define BSLBannerNotificationAlpha 0.95

@protocol BSLBannerNotificationDelegate <NSObject>

@optional
-(CGFloat) navigationbarBottomOfViewController:(UIViewController*)viewController;

@end

@interface BSLBannerNotificationView : UIView

@property (nonatomic,readonly) NSString* title;
@property (nonatomic,readonly) NSString* subTitle;
@property (nonatomic,readonly) UIViewController* viewController;
@property (nonatomic,assign) CGFloat duration;
@property (nonatomic,assign) BSLNotificationPosition position;
@property (nonatomic,assign) BOOL messageIsFullyDisplayed;
@property (nonatomic,assign) id<BSLBannerNotificationDelegate> delegate;

-(instancetype) initWithTitle:(NSString*)title
                     subtitle:(NSString*)subTitle
                        image:(UIImage*)image
                         type:(BSLNotificationType)notificationType
                     duration:(CGFloat)duration
             inViewController:(UIViewController*)viewController
                     callback:(void (^)())callback
                  buttonTitle:(NSString*)buttonTitle
               buttonCallback:(void (^)())buttonCallback
                   atPosition:(BSLNotificationPosition)position
         canBeDismissedByUser:(BOOL)dismissingEnabled;

-(void) fadeMeOut;

+(void) addNotificationDesignFromFile:(NSString*)file;

@end
